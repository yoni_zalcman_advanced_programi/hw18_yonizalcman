#ifndef _HELPER_H
#define _HELPER_H

#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <sstream>

using std::vector;
using std::string;
using std::istream_iterator;

// this is service class with static functions
class Helper
{
public:
	// Remove whitespace from the begining and the end of the string
	static void trim(string &str);

	// Get a vector of strings, and return a vector of words 
	// ("for example" vec[0]:"for" vec[1]:"example"
	static vector<string> get_words(string &str);



private:
	// Remove whitespace from the end of the string
	static void rtrim(string &str);  

	// Remove whitespace from the beginning of the string
	static void ltrim(string &str); 


};

#endif