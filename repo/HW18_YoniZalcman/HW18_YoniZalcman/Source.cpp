#include<stdio.h>
#include<iostream>
#include<Windows.h>
#include<tchar.h>
#include<string>
#include "Helper.h"

#define unsigned long DWORD;

using namespace std;

void main()
{
	int line = 1;
	string input="";
	vector<string>command;
	DWORD path_max=2048;
	char s[100];
	while (true)
	{
		cout << ">>";
		cin >> input;
		command = Helper::get_words(input);
		if (command[0] == "pwd")
		{
			GetCurrentDirectory(path_max, s);
			cout << s << endl;
		}
		else if (command[0] == "cd")
		{
			char* newPath = (char*)command[1].c_str();
			SetCurrentDirectory(newPath);
		}
	}
}